const User = require("../model/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { generateAuthOTP } = require("../utility/otp");
const UserOtpVerification = require("../model/userOtpVerification");

export const showMessage = (req, res) => {
  res.status(200).send(req.params.message);
};

export const register = async (req, res) => {
  const { name, email, photo, password, affiliation, qualification, age } = req.body;
  if (!name) return res.status(400).send("Name is required");

  //password length validation for debugging
  if (!password || password.length < 6)
    return res.status(400).send("Password should be a minimum of 6 characters");
  
  try {
    const userExist = await User.findOne({ email } );
    if (userExist) return res.status(400).json({ error: "Email is taken" });

    const otp = await generateAuthOTP({email});
    if(!otp){
      return res.status(400).json({status:"Failed",message:"OTP not generated"})
    }

    // Create a new user instance
    const newUser = await User.create({ name, email, photo, password, affiliation, qualification, age });
    if(!newUser){
      return res.status(400).json({status:"Failed",message:"User not created"})
    }

    res.status(201).json({ status: "success", data: newUser });
  } catch (error) {
      console.error(error);
      res.status(400).send({
        status: "failed",
        message:error.message
      })
  }
};

export const login = async (req, res) => {
  try {
    const user = await User.findByCredentials(req.body.email, req.body.password);
      if(!user.verified){
          res.status(400).json({status:"Failed",message:"User not verified"})
      }else{
          if(user){
              const AuthToken = await user.generateAuthToken();
              res.cookie("session_id",AuthToken)
              
              res.status(200).json({message:"Credentail Matched",status:"Success", token: AuthToken})
          }else{
              res.status(400).json({message:"Invalid Credential",status:"Failed"})
          }
      }
  } catch (e) {
      res.status(400).send(e)
  }
}

export const verifyOTP = async (req, res) => {
  try {
      let {email,otp} = req.body;

      if(!email || !otp){
          throw Error("Empty otp details are not allowed.")
      }else{
          const userOTPVerificationRecords = await UserOtpVerification.find({email})

          if(userOTPVerificationRecords.length <= 0) {
              throw new Error("Account record doesn't exists or has been verified already. Please signup or login.");
          }else{
              const {expiresAt} = userOTPVerificationRecords[0].expiresAt;
              const hashedOTP = userOTPVerificationRecords[0].otp;

              if (expiresAt < Date.now()){
                UserOtpVerification.deleteMany({email});
                  throw new Error("OTP has expired. Please request again.");
              }else{
                  const validOTP = await bcrypt.compare(otp,hashedOTP);
                  if(!validOTP){
                      throw new Error("Invalid code passed. check your inbox.")
                  }else{
                      await User.updateOne({email},{verified:true});
                      await UserOtpVerification.deleteMany({email});

                      res.json({
                          status:"Verified",
                          message:"User email verified successfully."
                      });
                  }
              }
          }
      }
  } catch (e) {
      res.status(400).send({
          status:"Failed",
          message:e.message
      })
  }
}


export const getUser = async (req, res) => {
  try {
    // Use findById for a more straightforward query
    const user = await User.findById(req.params._id);
    
    if (!user) {
      return res.status(404).json({ status: "fail", message: "User not found" });
    }

    res.status(200).json({ status: "success", data: { user } });
  } catch (error) {
    console.error("Error fetching doctor:", error);
    res.status(500).json({ status: "error", message: "Server error" });
  }
};
export const getdoctorsbyToken = async (req, res) => {
  try {
    const token = req.params.token;

    // Use $elemMatch to find a document with the matching token in the tokens array
    const user = await User.findOne({ tokens: { $elemMatch: { token } } });

    if (!user) {
      return res.status(404).json({ status: "fail", message: "Doctor not found" });
    }

    res.status(200).json({ status: "success", data: { user } });
  } catch (error) {
    console.error("Error fetching doctor by token:", error);
    res.status(500).json({ status: "error", message: "Server error" });
  }
};
export const logout = async (req, res) => {

  try {
      // Remove the current token from the user's tokens array
      await req.user.removeToken(req.token);
      // Clear the session cookie
      res.clearCookie("session_id");
      res.status(200).json({ message: "Logout successful", status: "Success" });
  } catch (e) {
      res.status(500).json({ message: "Internal server error", status: "Failed" });
  }
}

export const getAccountAddress = async (req, res) => {
  try {
    // Find the patient by the token in the request parameters
    const user = await User.findOne({ 'tokens.token': req.params.token });
    await user.update({accountAddress: req.body.accountAddress})
    res.status(200).json({ status: "success", data: { user } });
} catch (error) {
    console.error("Error fetching user account address:", error);
    res.status(500).json({ status: "error", message: error.message });
  }
}



