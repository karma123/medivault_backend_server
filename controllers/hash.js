const Hash = require('../model/ipfsHash');

export const storeHash = async(req, res) => {
    try {
        const { hash } = req.body;
        const newHash = await Hash.create({hash});
        if(!newHash){
            return res.status(400).json({status:"Failed",message:"Hash not stored"})
        }
        res.status(201).json({ status: "success", data: newHash });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message:error.message
        })
    }
}

export const getHash = async(req, res) => {
    try{
        const hash = await Hash.find();
        if(!hash){
            return res.status(400).json({status:"Failed",message:"Hash not found"})
        }
        res.status(200).json({ status: "success", data: hash });
    }catch(error){
        console.error(error);
        res.status(400).send({
            status: "failed",
            message:error.message
        })
    }
}

export const getHashById = async(req, res) => {
    try{
        const hash = await Hash.findById(req.params.id);
        if(!hash){
            return res.status(400).json({status:"Failed",message:"Hash not found"})
        }
        res.status(200).json({ status: "success", data: hash });
    }
    catch(error){
        console.error(error);
        res.status(400).send({
            status: "failed",
            message:error.message
        })
    }
}
export const deleteHash = async(req, res) => {
    try{
        const hash = await Hash.deleteMany();
        if(!hash){
            return res.status(400).json({status:"Failed",message:"Hash not found"})
        }
        res.status(200).json({ status: "success", data: hash });
    }
    catch(error){
        console.error(error);
        res.status(400).send({
            status: "failed",
            message:error.message
        })
    }
} 

export const deleteHashById = async(req, res) => {
    try{
        const hash = await Hash.findByIdAndDelete(req.params.id);
        if(!hash){
            return res.status(400).json({status:"Failed",message:"Hash not found"})
        }
        res.status(200).json({ status: "success", data: hash });
    }
    catch(error){
        console.error(error);
        res.status(400).send({
            status: "failed",
            message:error.message
        })
    }
}
