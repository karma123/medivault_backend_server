
const Patient = require("../model/patient");
const User = require("../model/user")
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { generateAuthOTP } = require("../utility/patientOtp");
const PatientOtpVerification = require("../model/patientOtpVerification");

export const showMessage = (req, res) => {
  res.status(200).send(req.params.message);
};

export const register = async (req, res) => {
  const { name, email, photo, password, age } = req.body;
  if (!name) return res.status(400).send("Name is required");

  //password length validation for debugging
  if (!password || password.length < 6)
    return res.status(400).send("Password should be a minimum of 6 characters");
  
  try {
    const patientExist = await Patient.findOne({ email } );
    if (patientExist) return res.status(400).json({ error: "Email is taken" });

    const otp = await generateAuthOTP({email});
    if(!otp){
      return res.status(400).json({status:"Failed",message:"OTP not generated"})
    }

    // Create a new user instance
    const newPatient = await Patient.create({ name, email, photo, password, age });
    if(!newPatient){
      return res.status(400).json({status:"Failed",message:"User not created"})
    }

    res.status(201).json({ status: "success", data: newPatient });
  } catch (error) {
      console.error(error);
      res.status(400).send({
        status: "failed",
        message:error.message
      })
  }
};

export const login = async (req, res) => {
  try {
    const patient = await Patient.findByCredentials(req.body.email, req.body.password);
      if(!patient.verified){
          res.status(400).json({status:"Failed",message:"User not verified"})
      }else{
          if(patient){
              const AuthToken = await patient.generateAuthToken();
              // console.log(AuthToken)
              res.cookie("session_id", AuthToken)

              res.status(200).json({message:"Credentail Matched",status:"Success", token: AuthToken})
          }else{
              res.status(400).json({message:"Invalid Credential",status:"Failed"})
          }
      }
  } catch (e) {
      res.status(400).send(e)
  }
}

export const verifyOTP = async (req, res) => {
  try {
      let {email,otp} = req.body;

      if(!email || !otp){
          throw Error("Empty otp details are not allowed.")
      }else{
          const patientOTPVerificationRecords = await PatientOtpVerification.find({email})

          if(patientOTPVerificationRecords.length <= 0) {
              throw new Error("Account record doesn't exists or has been verified already. Please signup or login.");
          }else{
              const {expiresAt} = patientOTPVerificationRecords[0].createdAt;
              const hashedOTP = patientOTPVerificationRecords[0].otp;

              if (expiresAt < Date.now()){
                PatientOtpVerification.deleteMany({email});
                  throw new Error("OTP has expired. Please request again.");
              }else{
                  const validOTP = await bcrypt.compare(otp,hashedOTP);
                  if(!validOTP){
                      throw new Error("Invalid code passed. check your inbox.")
                  }else{
                      await Patient.updateOne({email},{verified:true});
                      await PatientOtpVerification.deleteMany({email});

                      res.json({
                          status:"Verified",
                          message:"User email verified successfully."
                      });
                  }
              }
          }
      }
  } catch (e) {
      res.status(400).send({
          status:"Failed",
          message:e.message
      })
  }
}

export const getPatientbyToken = async (req, res) => {
  try {
    const token = req.params.token;

    // Use $elemMatch to find a document with the matching token in the tokens array
    const patient = await Patient.findOne({ tokens: { $elemMatch: { token } } });

    if (!patient) {
      return res.status(404).json({ status: "fail", message: "Patient not found" });
    }

    res.status(200).json({ status: "success", data: { patient } });
  } catch (error) {
    console.error("Error fetching patient by token:", error);
    res.status(500).json({ status: "error", message: "Server error" });
  }
};



export const getPatient = async (req, res) => {
  try {
    // Use findById for a more straightforward query
    const patient = await Patient.findById(req.params._id);
    
    if (!patient) {
      return res.status(404).json({ status: "fail", message: "Patient not found" });
    }

    res.status(200).json({ status: "success", data: { patient } });
  } catch (error) {
    console.error("Error fetching patient:", error);
    res.status(500).json({ status: "error", message: "Server error" });
  }
};

export const authoriseDoctor = async (req, res) => {
  try {
    const { email } = req.body;
    const { id } = req.params;

    // Find the patient by email
    const patient = await Patient.findOne({ email });
    if (!patient) {
      return res.status(404).json({ status: "error", message: "Patient not found" });
    }

    // Find the doctor by ID
    const doctor = await User.findById(id);
    if (!doctor) {
      return res.status(404).json({ status: "error", message: "Doctor not found" });
    }

    const docID = doctor._id;
    const patientID = patient._id;

    // Update the patient with the doctor's ID and the doctor with the patient's ID in parallel
    await Promise.all([
      Patient.updateOne({ email }, { $addToSet: { doctors: docID } }),
      User.updateOne({ _id: id }, { $addToSet: { patients: patientID } })
    ]);

    // Refetch the updated patient and doctor
    const updatedPatient = await Patient.findOne({ email });
    const updatedDoctor = await User.findById(id);

    res.status(200).json({ status: "success", doctor: updatedDoctor, patient: updatedPatient });

  } catch (error) {
    console.error("Error authorising doctor:", error);
    res.status(500).json({ status: "error", message: error.message });
  }
};


export const revokeDoc = async (req, res) => {
  try {
    const { email } = req.body;
    const { id } = req.params;

    // Find the doctor by ID
    const doctor = await User.findById(id);
    if (!doctor) {
      return res.status(404).json({ status: "error", message: "Doctor not found" });
    }

    // Find the patient by email
    const patient = await Patient.findOne({ email });
    if (!patient) {
      return res.status(404).json({ status: "error", message: "Patient not found" });
    }

    const docId = doctor._id;
    const patientId = patient._id;

    // Update the patient to remove the doctor from their doctors array
    const patientUpdatePromise = Patient.updateOne(
      { email },
      { $pull: { doctors: docId } }
    );

    // Update the doctor to remove the patient from their patients array
    const doctorUpdatePromise = User.updateOne(
      { _id: id },
      { $pull: { patients: patientId } }
    );

    // Execute both updates in parallel
    await Promise.all([patientUpdatePromise, doctorUpdatePromise]);

    // Refetch the updated patient and doctor
    const updatedPatient = await Patient.findOne({ email });
    const updatedDoctor = await User.findById(id);

    res.status(200).json({ status: "success", doctor: updatedDoctor, patient: updatedPatient });
  } catch (error) {
    console.error("Error revoking doctor:", error);
    res.status(500).json({ status: "error", message: error.message });
  }
};

export const getAllAuthorised = async (req, res) => {
  try {
    // Find the patient by the token in the request parameters
    const patient = await Patient.findOne({ 'tokens.token': req.params.token });
    if (!patient) {
      return res.status(404).json({ status: "error", message: "Patient not found" });
    }

    const patientId = patient._id;

    // Find all doctors who have the patient in their patients array
    const authorisedDoctors = await User.find({
      patients: patientId,
      verified: true
    });

    // Extract the IDs of the authorised doctors
    const userIds = authorisedDoctors.map(user => user._id);

    // Respond with the user IDs and the authorised doctor details
    res.status(200).json({ status: "success", userIds, authorisedDoctors });
  } catch (error) {
    console.error("Error fetching authorised doctors:", error);
    res.status(500).json({ status: "error", message: error.message });
  }
};


export const getAllUnauthorised = async (req, res) => {
  try {
    // Find the patient by the token in the request parameters
    const patient = await Patient.findOne({ 'tokens.token': req.params.token });
    if (!patient) {
      return res.status(404).json({ status: "error", message: "Patient not found" });
    }

    const patientId = patient._id;

    // Find all verified doctors who do not have the patient in their patients array
    const unauthorisedDoctors = await User.find({
      patients: { $ne: patientId },
      verified: true
    });

    // Extract the IDs of the unauthorised doctors
    const userIds = unauthorisedDoctors.map(user => user._id);

    // Respond with the user IDs and the unauthorised doctor details
    res.status(200).json({ status: "success", userIds, unauthorisedDoctors });
  } catch (error) {
    console.error("Error fetching unauthorised doctors:", error);
    res.status(500).json({ status: "error", message: error.message });
  }
};

export const getAccountAddress = async (req, res) => {
  try {
    // Find the patient by the token in the request parameters
    const patient = await Patient.findOne({ 'tokens.token': req.params.token });
    await patient.update({accountAddress: req.body.accountAddress})
    res.status(200).json({ status: "success", data: { patient } });
} catch (error) {
    console.error("Error fetching patient account address:", error);
    res.status(500).json({ status: "error", message: error.message });
  }
}
