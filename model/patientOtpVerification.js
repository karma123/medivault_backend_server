const mongoose = require('mongoose');

const patientOtpVerificationSchema = new mongoose.Schema({
  email: {
    type: String,
    ref: 'Patient', // Reference to the Institute model
    required: true,
  },
  otp: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  expiresAt: {
    type: Date,
    required: true,
  },
});

// Create the UserOtpVerification model
const PatientOtpVerification = mongoose.model('PatientOtpVerification', patientOtpVerificationSchema);

module.exports = PatientOtpVerification;
