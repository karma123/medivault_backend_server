const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require("jsonwebtoken");

const patientSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Enter your name']
    },
    age:{
        type: Number,
    },
    email: {
        type: String,
        required: [true, 'Please enter your Email ID'],
        unique: true,
        validate: [validator.isEmail, 'Please enter a valid Email ID']
    },
    accountAddress:{
        type: String
    },
    photo: {
        type: String,
        default: 'default.jpg',
    },
    password: {
        type: String,
        required: [true, 'Please provide a password'],
        minlength: 8,
        //Password wont be included when we get the patients
        select: false,
    },
    passwordResetToken:{
        type:String,
        default:"nil"
    },
    verified:{
        type:Boolean,
        default:false
    },
    doctors:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    tokens:[{
        token:{
            type: String,
            require: true
        }
    }]
})

patientSchema.methods.generateAuthToken = async function (){
    const patient = this
    const token = jwt.sign({_id:patient._id.toString()},process.env.TOKEN_SIGNATURE,{expiresIn:process.env.JWT_EXPIRES_IN})

    patient.tokens = patient.tokens.concat({token:token})
    await patient.save()

    return token
}

patientSchema.methods.generateToken = async function(){
    const patient = this;
    const token = jwt.sign({_id:patient._id.toString()},process.env.TOKEN_SIGNATURE,{expiresIn:process.env.PASSWORD_RESET_JWT_EXPIRES_IN})

    return token;
}

patientSchema.statics.findByCredentials = async (email, password) => {
    const patient = await Patient.findOne({email}).select('+password')
    try{
    if (!patient){
        throw "Invalid Credential 1"
    }
    const isMatch = await bcrypt.compare(password, patient.password);

    if (!isMatch) {
        throw "Password is incorrect"
    }
    return patient
    
    } catch (error) {
        console.error("Error in findByCredentials",error)
        throw error;
    }
}


patientSchema.pre('save', async function (next) {
    // Only runs this code if password is modified
    if (!this.isModified('password')) return next()

    //hash the paswword with the cost of 12
    this.password = await bcrypt.hash(this.password, 12)
    next()
})

patientSchema.pre('findOneAndUpdate', async function (next) {
    const update = this.getUpdate();
    if (update.password !== '' &&
        update.password !== undefined &&
        update.password == update.passwordConfirm) {
        //hash the paswword with the cost of 12
        this.password = await bcrypt.hash(this.password, 12)

        //delete confirmedpassword field
        this.passwordConfirm = undefined
        next()
    } else
        next()
})

patientSchema.methods.correctPassword = async function (
    candidatePassword,
    patientPassword
) {
    return await bcrypt.compare(candidatePassword, patientPassword)
}

const Patient = mongoose.model('Patien', patientSchema)
module.exports = Patient