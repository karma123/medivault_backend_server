const mongoose = require('mongoose');

const hashSchema = new mongoose.Schema({
    hash: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});


const Hash = mongoose.model('Hash', hashSchema);
module.exports = Hash;