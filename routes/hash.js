import express from 'express';

const hashController = require('../controllers/hash.js');

const router = express.Router();

router.post('/storeHash', hashController.storeHash);
router.get('/getHash', hashController.getHash);
router.get('/getHash/:id', hashController.getHashById);
router.delete('/deleteHash/:id', hashController.deleteHashById);
router.delete('/deleteAllHash', hashController.deleteHash)

module.exports = router;