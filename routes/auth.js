import express from "express";

const router = express.Router();

const userController = require("../controllers/auth");
const {userAuth} = require("../middleware/auth")

router.post("/register", userController.register);
router.post("/login", userController.login);    
router.post("/verify", userController.verifyOTP);

router.get("/:message", userController.showMessage);
router.get("/getuser/:id", userController.getUser)
router.get("/getuserbytoken/:token", userController.getdoctorsbyToken)
router.get("/logout", userController.logout)
router.patch("/accountAddress/:token", userController.getAccountAddress)
// export default router;
//export the router so thatit can be used in other parts of your application
module.exports = router;
