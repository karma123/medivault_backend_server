import express from "express";

const patientController = require("../controllers/patientAuth.js");
const router = express.Router();

router.post("/register", patientController.register);
router.post("/login", patientController.login);
router.post("/verify", patientController.verifyOTP);

router.get("/:message", patientController.showMessage);
router.get("/getpatient/:_id", patientController.getPatient);
router.get("/getpatientbytoken/:token", patientController.getPatientbyToken);
router.post("/authoriseDoc/:id", patientController.authoriseDoctor)
router.post("/revokeDoc/:id", patientController.revokeDoc)
router.get("/getAllUnauthorised/:token", patientController.getAllUnauthorised)
router.get("/getAllAuthorised/:token", patientController.getAllAuthorised)
// router.get("/alldoc", patientController.getAllDoctors)
router.patch("/accountAddress/:token", patientController.getAccountAddress)

module.exports = router;