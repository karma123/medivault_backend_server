const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config({ path: './.env' })

const DB = process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD)

const authRouter = require('./routes/auth')
const patientRouter = require('./routes/patientAuth')
const hashRouter = require('./routes/hash')

app.use(cors())
app.use(express.json())
app.use('/api/auth', authRouter)
app.use('/api/patient', patientRouter)
app.use('/hash', hashRouter)

mongoose.connect(DB).then((con) => {
    // console.log(con.connections)
    console.log('DB connection successful')
}).catch(error => console.log(error));

// Starting the server on port 4001
const port = process.env.PORT || 4001
app.listen(port, () => {
    console.log(`App running ${port}`)
})
